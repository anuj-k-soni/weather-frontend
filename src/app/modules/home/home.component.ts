import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EnumOperations, appid } from 'src/app/shared/models/city-enum';
import { ResultData } from 'src/app/shared/models/weatherapi-resonse.model';
import { CustomDatePipe } from 'src/app/shared/pipes/custom-date.pipe';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(
    private readonly homeService: HomeService,
    private customDatePipe: CustomDatePipe,
    private readonly router: Router
  ) {}
  weatherData: any;
  ngOnInit(): void {
    this.getWeatherData();
    /* this.weatherData = [
      {name:'paris',main:{temp:23},sys:{sunrise:1243566,sunset:12712712} ,id:1},
      {name:'london',main:{temp:23},sys:{sunrise:1243566,sunset:12712712} ,id:4},
      {name:'new york',main:{temp:23},sys:{sunrise:1243566,sunset:12712712} ,id:3},
      {name:'delhi',main:{temp:23},sys:{sunrise:1243566,sunset:12712712} ,id:2}
    ] */
  }

  getWeatherData() {
    this.homeService.getWeatherData().subscribe(
      (data) =>
        (this.weatherData = data.map((d: ResultData) => {
          d.sys.sunrise = this.customDatePipe.convert(
            d.sys.sunrise,
            undefined,
            `Europe/${d.name}`
          );
          d.sys.sunset = this.customDatePipe.convert(
            d.sys.sunset,
            undefined,
            `Europe/${d.name}`
          );
          return d;
        }))
    );
  }

  navigateWithData(city: string) {
    let c: string = '';
    let eo = new EnumOperations()
      eo.getEuropeanCityCntryCodes()
      .forEach((cityCountryCode) => {
        if (cityCountryCode.split(',')[0] === city.toLowerCase()) {
          c = cityCountryCode;
        }
      });
    this.router.navigate(['/city-forcast'], {
      queryParams: { city: c, appid: appid },
    });
  }
}
