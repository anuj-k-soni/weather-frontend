import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { BrowserModule } from '@angular/platform-browser';
import { HomeRoutingModule } from './home-routing.module';
import { CustomDatePipe } from 'src/app/shared/pipes/custom-date.pipe';
import { CityComponent } from './city/city.component';
import { ForcastComponent } from './forcast/forcast.component';



@NgModule({
  declarations: [
    HomeComponent,
    CityComponent,
    ForcastComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  providers:[
    CustomDatePipe
  ]
})
export class HomeModule { }
