import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import {
  ResultData,
  Forecast,
} from 'src/app/shared/models/weatherapi-resonse.model';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-forcast',
  templateUrl: './forcast.component.html',
  styleUrls: ['./forcast.component.css'],
})
export class ForcastComponent implements OnInit {
  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly homeService: HomeService
  ) {}
  forcastData: Forecast;
  ngOnInit(): void {
    this.getForcastForCity(this.activatedRoute.snapshot.queryParams);
  }

  getForcastForCity(queryParams): void {
    this.homeService.getForcastData(queryParams).subscribe(
      (data: Forecast) => {
        this.filterData(data);
      },
      (error: Error) => console.log(error)
    );
  }

  filterData(data:Forecast){
    let updatedList = data.list.filter(function(x){
      let dt = moment(moment(x.dt_txt).format('YYYY-MM-DD HH:mm:ss')).hours();
      return dt == 9 ?  true :  false;
      })
    this.forcastData = {...data,list:updatedList};
  }
}
  
