import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForcastComponent } from './forcast/forcast.component';
import { HomeComponent } from './home.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {path:'city-forcast',component:ForcastComponent},
            {
                path:'',
                component:HomeComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
