import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, from, of } from 'rxjs';
import {
  appid,
  EnumOperations,
  weatherapi,
  forecasturl
} from '../../shared/models/city-enum';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  constructor(private readonly httpClient: HttpClient) {}
  
  getWeatherData() {
    return forkJoin(
      new EnumOperations().getEuropeanCityCntryCodes().map((city) => {
        return this.httpClient.get(weatherapi, {
          params: new HttpParams().set('appid', appid).set('q', city),
        });
      })
    );
  }
  getForcastData(params){
    const options = {
      headers : new HttpHeaders({
        'Accept':'application/json'
      }),
       params :  new HttpParams()
      .set('appid', params.appid)
      .set('q',params.city)
    }
    return this.httpClient.get(forecasturl,options);
  }

}
