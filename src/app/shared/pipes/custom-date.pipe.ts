import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import 'moment-timezone';
@Pipe({
  name: 'customDate'
})
export class CustomDatePipe extends DatePipe implements PipeTransform {

  convert(value: string |number , format: string = 'long', timezone: string): string {
    let formattedDate : string |  null = '';
    const timezoneOffset = moment.tz(moment.unix(Number(value)),timezone)
                                 .format('hh:mm:ss a');
    return timezoneOffset
  }

}
