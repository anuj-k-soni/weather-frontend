export interface ResultData {
  main: {
    feels_like: number;
    humidity: number;
    pressure: number;
    sea_level: number;
    temp: number;
    temp_max: number;
    temp_min: number;
  };
  sys: {
    country: string;
    id: number;
    sunrise: number | string;
    sunset: number | string;
  };
  id: number;
  name: string;
  timezone: number;
  dt_txt:string;
}

export interface Forecast {
  city: { name: string };
  list: ResultData[];
}
