
export const appid = '7b6ecf2decc494e58b3e0cc0fa64c2ed';
export const weatherapi = 'http://api.openweathermap.org/data/2.5/weather';
export const forecasturl = 'http://api.openweathermap.org/data/2.5/forecast';

export enum EuropeanCities {
  'Europe/Paris' = 'paris,fr',
  'Europe/Vienna' = 'vienna,at',
  'Europe/Amsterdam' = 'amsterdam,nl',
  'Europe/London' = 'london,uk',
  'Europe/Prague' = 'prague,cz',
}

export class EnumOperations {
  getEuropeanCities(): string[] {
    return Object.keys(EuropeanCities);
  }
  getEuropeanCityCntryCodes(): string[] {
    return Object.keys(EuropeanCities).map((key) => EuropeanCities[key]);
  }
}
